'use strict'

import { app, BrowserWindow, ipcMain } from 'electron'
import fs from 'fs'
import path from 'path'
import * as _ from 'lodash'

import { Helpers } from './helpers'
import { pdfbox } from './modules/PdfBox'

const appFolders = {
  home: path.join(app.getPath('documents'), '.PDFToolsV3'),
  concatenados: path.join(app.getPath('documents'), '.PDFToolsV3', '.concatenados'),
  separados: path.join(app.getPath('documents'), '.PDFToolsV3', '.separados'),
  encriptados: path.join(app.getPath('documents'), '.PDFToolsV3', '.encriptados'),
  desencriptados: path.join(app.getPath('documents'), '.PDFToolsV3', '.desencriptados'),
  imagens: path.join(app.getPath('documents'), '.PDFToolsV3', '.imagens'),
  assinados: path.join(app.getPath('documents'), '.PDFToolsV3', '.assinados')
}

global.sharedObj = { appFolders: appFolders }

let mainWindow = null

const winURL = process.env.NODE_ENV === 'development'
    ? `http://localhost:${require('../../../config').port}`
    : `file://${__dirname}/index.html`

function createWindow () {
  mainWindow = new BrowserWindow({
    minHeight: 600,
    height: 700,
    minWidth: 1000,
    width: 1000,
    show: false
        // frame: false
  })

  mainWindow.loadURL(winURL)

  mainWindow.on('ready-to-show', () => {
    mainWindow.show()
  })

  mainWindow.on('closed', () => {
    mainWindow = null
  })

    // eslint-disable-next-line no-console
  console.log('mainWindow opened')
}

app.on('ready', () => {
  Helpers.createAppFoldersIfNotExist(appFolders)
  createWindow()
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

let concatenarFilesIsDisabled = false
ipcMain.on('concatenarFiles', function (event, srcFiles) {
  if (!concatenarFilesIsDisabled) {
    concatenarFilesIsDisabled = true
    let time = (new Date()).getTime()
    let name = `${time}.pdf`
    let destFile = path.join(appFolders.concatenados, name)

    pdfbox.concatenar(srcFiles, destFile, function (err) {
      if (err) {
        console.log(err)
        event.sender.send('error', err)
      } else {
        let stats = fs.statSync(destFile)

        event.sender.send('concatenado', {name: name, path: destFile, size: stats.size, createdAt: stats.birthtime})
      }
      concatenarFilesIsDisabled = false
    })
  }
})

let encriptarFileIsDisabled = false
ipcMain.on('encriptarFiles', function (event, password, srcFiles) {
  if (!encriptarFileIsDisabled) {
    encriptarFileIsDisabled = true
    _.forEach(srcFiles, (srcFile) => {
      let extParts = srcFile.split(path.sep)
      let time = (new Date()).getTime()
      let name = time + '-' + extParts[extParts.length - 1]
      let destFile = path.join(appFolders.encriptados, name)

      pdfbox.encriptar(password, srcFile, destFile, function (err) {
        if (err) {
          console.log(err)
          event.sender.send('error', err)
        } else {
          let stats = fs.statSync(destFile)

          event.sender.send('encriptado', {name: name, path: destFile, size: stats.size, createdAt: stats.birthtime})
        }
      })
      encriptarFileIsDisabled = false
    })
  }
})

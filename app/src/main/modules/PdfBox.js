import fs from 'fs'
import childProcess from 'child_process'
import path from 'path'

export const pdfbox = {
  getJarPath () {
    if (process.env.NODE_ENV === 'development') {
      return path.join(__dirname, '../', 'jar', 'pdfbox.jar')
    }
    else {
      return path.join(__dirname, '../', '../', 'app.asar.unpacked', 'jar', 'pdfbox.jar')
    }
  },
  fileExists (filePath) {
    try {
      return fs.statSync(filePath).isFile()
    } catch (err) {
      return false
    }
  },
  checkSrcFiles (src, callback) {
    if (!Array.isArray(src)) { return callback('Source is not an Array') } else if (src.length < 2) { return callback('There must be atleast 2 input files') }

    let normSrc = []

    for (let i = 0; i < src.length; i++) {
      if (typeof (src[i]) === 'string') {
          /*
           //Check if source file exists

           fs.stat(src[i],function(err,stats){

           if(err)
           return callback('Can\'t access file : ' + src[i]);

           if(!stats.isFile())
           return callback(src[i] + ' is not a File');

           }); */

        normSrc.push(`"${src[i]}"`)
      } else {
        return callback(`Source : ${src[i]}, is not a file name`)
      }
    }

    callback(null, normSrc)
  },
  checkSrc (filePath, callback) {
    if (!this.fileExists(filePath)) { return callback('File not exist') }

    let basename = path.basename(filePath)

    if (!path.extname(basename) === '.pdf') { return callback('File is not a PDF') }

    let normSrc = ''

    if (typeof (filePath) === 'string') { normSrc = `"${filePath}"` } else { return callback(`Source : ${filePath} + , is not a file name`) }

    callback(null, normSrc)
  },
  checkPassword (password, callback) {
    if (!password) { return callback('Password invalid') }

    let normPassword = ''

    if (typeof (password) === 'string') { normPassword = `"${password}"` } else { return callback(`Password : ${password} + , is not valid`) }

    callback(null, normPassword)
  },
  concatenar (srcFiles, destFile, callback) {
    let jarPath = this.getJarPath()

    // prepare command line
    let command = [`java -jar "${jarPath}" PDFMerger`]

    // check if each file in srcFiles is a File
    this.checkSrcFiles(srcFiles, function (err, normSrc) {
      // width any error return
      if (err) { return callback(err) }

      // widthout error continue

      // create de final file sync
      fs.writeFileSync(destFile)

      // add the rest of command line
      command = command.concat(normSrc)
      command.push(`"${destFile}"`)

      let child = childProcess.exec(command.join(' '), function (err, stdout, stderr) {
        if (err) { return callback(err) }

        callback(null)
      })

      child.on('error', function (err) {
        return callback(`Execution problem. ${err}`)
      })
    })
  },
  separar (filePath, callback) {
    let jarPath = this.getJarPath()

    let command = [`java -jar "${jarPath}" PDFSplit`]

    this.checkSrc(filePath, function (err, normSrc) {
      if (err) { return callback(err) }

      command = command.concat(normSrc)

      let child = childProcess.exec(command.join(' '), function (err, stdout, stderr) {
        if (err) { return callback(err) }

        callback(null)
      })

      child.on('error', function (err) {
        return callback(`Execution problem. ${err}`)
      })
    })
  },
  encriptar (password, filePath, destFile, callback) {
    let jarPath = this.getJarPath()
    let self = this

    // prepare command line
    let command = [`java -jar "${jarPath}" Encrypt`]

    // check if each file in srcFiles is a File
    this.checkSrc(filePath, function (err, normSrc) {
        // width any error return
      if (err) { return callback(err) }

        // widthout error continue
      self.checkPassword(password, function (err, normPass) {
        if (err) { return callback(err) }

        // create de final file sync
        fs.writeFileSync(destFile)

                // add the rest of command line
        command.push(`-O "${normPass}" -U "${normPass}"`)
        command.push(normSrc)
        command.push(`"${destFile}"`)

        let child = childProcess.exec(command.join(' '), function (err, stdout, stderr) {
          if (err) { return callback(err) }

          callback(null)
        })

        child.on('error', function (err) {
          return callback(`Execution problem. ${err}`)
        })
      })
    })
  }
}

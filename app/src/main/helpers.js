import fs from 'fs'
import fsWin from 'fswin'
import * as _ from 'lodash'

export const Helpers = {
  dirExists (dirPath) {
    try { return fs.statSync(dirPath).isDirectory() } catch (err) { return false }
  },
  createAppFoldersIfNotExist (appFolders) {
    let self = this
    _.forEach(appFolders, function (folder) {
      if (!self.dirExists(folder)) {
        if (process.platform === 'win32' || process.platform === 'win64') {
          fs.mkdirSync(folder)
          fsWin.setAttributesSync(folder, {IS_HIDDEN: true})
        }
        else {
          fs.mkdirSync(folder)
        }
      }
    })
  }
}

export default {
  addFileToList ({ commit }, file) {
    commit('addFileToList', file)
  },
  removeFileFromList ({ commit }, index) {
    commit('removeFileFromList', index)
  },
  moveFileUpInList ({ commit }, index) {
    commit('moveFileUpInList', index)
  },
  moveFileDownInList ({ commit }, index) {
    commit('moveFileDownInList', index)
  },
  removeAllFilesFromList ({ commit }) {
    commit('removeAllFilesFromList')
  },
  addFileToConcatenados ({ commit }, file) {
    commit('addFileToConcatenados', file)
  },
  removeFileFromConcatenados ({ commit }, index) {
    commit('removeFileFromConcatenados', index)
  },
  addFileToEncriptados ({ commit }, file) {
    commit('addFileToEncriptados', file)
  },
  removeFileFromEncriptados ({ commit }, index) {
    commit('removeFileFromEncriptados', index)
  }
}

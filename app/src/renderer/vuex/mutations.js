export default {
  removeAllFilesFromList (state) {
    state.filesInList.files = []
  },
  addFileToList (state, file) {
    state.filesInList.files.push({file})
  },
  removeFileFromList (state, index) {
    state.filesInList.files.splice(index, 1)
  },
  moveFileUpInList (state, index) {
    state.filesInList.move(index, index - 1)
  },
  moveFileDownInList (state, index) {
    state.filesInList.move(index, index + 1)
  },
  addFileToConcatenados (state, file) {
    state.filesConcatenados.unshift(file)
  },
  removeFileFromConcatenados (state, index) {
    state.filesConcatenados.splice(index, 1)
  },
  addFileToEncriptados (state, file) {
    state.filesEncriptados.unshift(file)
  },
  removeFileFromEncriptados (state, index) {
    state.filesEncriptados.splice(index, 1)
  }
}

window.addEventListener('dragover', function (e) {
  e = e || event
  e.preventDefault()
}, false)
window.addEventListener('drop', function (e) {
  e = e || event
  e.preventDefault()
}, false)

import Vue from 'vue'
import Electron from 'vue-electron'
import Resource from 'vue-resource'
import Router from 'vue-router'
import { store } from './vuex/store'
import moment from 'moment'
moment.locale('pt')

import '../assets/bootstrap/css/bootstrap.css'
import '../assets/font-awesome/css/font-awesome.css'
import '../assets/animate/animate.css'

import App from './App'
import routes from './routes'

Vue.use(Electron)
Vue.use(Resource)
Vue.use(Router)

Vue.config.debug = true

const router = new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes
})

/* eslint-disable no-new */
new Vue({
  router,
  store,
  ...App
}).$mount('#app')

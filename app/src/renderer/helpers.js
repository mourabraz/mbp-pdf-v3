import childProcess from 'child_process'

export const Helpers = {

  checkIfJavaIsOverMinimalVersion (callback) {
    let command = [`java -version`]

    let child = childProcess.exec(command.join(' '), function (err, stdout, stderr) {
      if (err) { return callback(err, null) }

      if (stderr) {
        let version = (stderr.split(' ')[2].split('"')[1]).substring(0, 3)
        callback(null, version > 1.6)
      }
    })

    child.on('error', function (err) {
      return callback(`Execution problem. ${err}`)
    })
  }

}

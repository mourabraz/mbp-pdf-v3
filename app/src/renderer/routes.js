export default [
  {
    path: '/',
    name: 'home',
    component: require('components/HomePageView')
  },
  {
    path: '/about',
    name: 'about',
    component: require('components/AboutPageView')
  },
  {
    path: '/settings',
    name: 'settings',
    component: require('components/SettingsPageView')
  },
  {
    path: '*',
    redirect: '/'
  }
]

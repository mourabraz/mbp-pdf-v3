import { filesInList } from './filesInList'
import { filesInFolders } from './filesInFolders'

export const state = {
  filesInList: filesInList,
  filesConcatenados: filesInFolders.getConcatenados(),
  filesEncriptados: filesInFolders.getEncriptados()
}

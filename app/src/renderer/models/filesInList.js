import * as _ from 'lodash'

export const filesInList = {
  files: [],
  move: function (oldIndex, newIndex) {
    while (oldIndex < 0) {
      oldIndex += this.files.length
    }
    while (newIndex < 0) {
      newIndex += this.files.length
    }
    if (newIndex >= this.files.length) {
      newIndex = 0
    }
    this.files.splice(newIndex, 0, this.files.splice(oldIndex, 1)[0])

    _.remove(this.files, function (n) {
      return n === undefined
    })
  }
}

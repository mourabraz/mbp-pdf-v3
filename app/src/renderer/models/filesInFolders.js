import electron from 'electron'
import path from 'path'
import fs from 'fs'
import * as _ from 'lodash'

const appFolders = electron.remote.getGlobal('sharedObj').appFolders

export const filesInFolders = {

  getConcatenados () {
    let files = fs.readdirSync(appFolders.concatenados)
    let filesArray = []

    _.orderBy(files, ['createdAt'], ['desc'])

    files.forEach(function (item) {
      let extParts = item.split('.')
      let ext = extParts[extParts.length - 1]
      if (ext === 'pdf') {
        let newPath = path.join(appFolders.concatenados, item)
        let stat = fs.statSync(newPath)
        let file = {
          name: item,
          path: newPath,
          size: stat.size,
          createdAt: stat.birthtime
        }
        filesArray.push(file)
      }
    })

    return filesArray
  },
  getEncriptados () {
    let files = fs.readdirSync(appFolders.encriptados)
    let filesArray = []

    _.orderBy(files, ['createdAt'], ['desc'])

    files.forEach(function (item) {
      let extParts = item.split('.')
      let ext = extParts[extParts.length - 1]
      if (ext === 'pdf') {
        let newPath = path.join(appFolders.encriptados, item)
        let stat = fs.statSync(newPath)
        let file = {
          name: item,
          path: newPath,
          size: stat.size,
          createdAt: stat.birthtime
        }
        filesArray.push(file)
      }
    })

    return filesArray
  }

}

import Config from 'electron-config'
let config = new Config()

export const notification = {
  title: 'Titulo',
  options: {
    body: 'Body',
    silent: !!config.get('notifications')
  },
  notify () {
    if (!('Notification' in window)) { alert('This browser does not support desktop notification') } else if (Notification.permission === 'granted') { this.makeNotification() } else if (Notification.permission !== 'denied') {
      Notification.requestPermission(function (permission) {
        if (permission === 'granted') { this.makeNotification() }
      })
    }
  },
  makeNotification () {
    return new Notification(this.title, this.options)
  }
}
